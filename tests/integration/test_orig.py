import requests
from test_setup import *
from common import *
from orig import *
from io import BytesIO as IO
import os

# Skip linting tests
# pylint: skip-file

TEST_STATE_STRING = "RUNNING"
TEST_RUNLOG_STRING = """2023-01-29T09:09:17.175Z: INIT
2023-01-29T10:39:15.192Z: RUNNING
2023-01-29T10:39:24.584Z: PAUSED
2023-01-29T10:39:28.959Z: RUNNING
2023-01-29T10:39:32.341Z: INIT
"""
TIMEOUT = 10

def test_orig_hostname_correct(serv_orig):
    assert HOSTNAME == "0.0.0.0"


def test_orig_correct_port(serv_orig):
    assert SERVERPORT_ORIG == 5081


def test_orig_state_displayed_correctly(serv_orig):
    file = open("orig_state.log", "w", encoding="utf-8")
    file.write(TEST_STATE_STRING)
    file.close()

    assert requests.get("http://localhost:5081/state", timeout=TIMEOUT).content == bytes(
        TEST_STATE_STRING, "utf-8"
    )


def test_orig_run_log_displayed_correctly(serv_orig):
    with open("orig_state_runlog.log", "w"):
        pass
    file = open("orig_state_runlog.log", "w", encoding="utf-8")
    file.write(TEST_RUNLOG_STRING)
    file.close()

    assert (
        bytes(TEST_RUNLOG_STRING, "utf-8")
        in requests.get("http://localhost:5081/run-log", timeout=TIMEOUT).content
    )
