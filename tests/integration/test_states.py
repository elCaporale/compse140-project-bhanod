import http
import requests
from test_setup import *
from common import *
from io import BytesIO as IO

# Skip linting tests
# pylint: skip-file
TIMEOUT = 10


def test_states_initial_correct(serv_orig):
    assert requests.get(
        "http://localhost:5081/state", timeout=TIMEOUT
    ).content == bytes("INIT", "utf-8")


def test_states_paused_correctly(serv_orig):
    requests.put("http://localhost:5081/state", "PAUSED", timeout=TIMEOUT)

    assert requests.get(
        "http://localhost:5081/state", timeout=TIMEOUT
    ).content == bytes("PAUSED", "utf-8")


def test_states_running_correctly(serv_orig):
    requests.put("http://localhost:5081/state", "RUNNING", timeout=TIMEOUT)

    assert requests.get(
        "http://localhost:5081/state", timeout=TIMEOUT
    ).content == bytes("RUNNING", "utf-8")


def test_states_shutdown_correctly(serv_orig):
    requests.put("http://localhost:5081/state", "SHUTDOWN", timeout=TIMEOUT)

    assert requests.get(
        "http://localhost:5081/state", timeout=TIMEOUT
    ).content == bytes("SHUTDOWN", "utf-8")


def test_states_transition_rejected_correctly(serv_orig):
    requests.put("http://localhost:5081/state", "SHUTDOWN1", timeout=TIMEOUT)

    assert requests.get(
        "http://localhost:5081/state", timeout=TIMEOUT
    ).content == bytes("INIT", "utf-8")


def test_states_multiple_transitions_correct(serv_orig):
    requests.put("http://localhost:5081/state", "RUNNING", timeout=TIMEOUT)

    assert requests.get(
        "http://localhost:5081/state", timeout=TIMEOUT
    ).content == bytes("RUNNING", "utf-8")

    requests.put("http://localhost:5081/state", "PAUSED", timeout=TIMEOUT)

    assert requests.get(
        "http://localhost:5081/state", timeout=TIMEOUT
    ).content == bytes("PAUSED", "utf-8")

    requests.put("http://localhost:5081/state", "RUNNING", timeout=TIMEOUT)

    assert requests.get(
        "http://localhost:5081/state", timeout=TIMEOUT
    ).content == bytes("RUNNING", "utf-8")

    requests.put("http://localhost:5081/state", "SHUTDOWN", timeout=TIMEOUT)

    assert requests.get(
        "http://localhost:5081/state", timeout=TIMEOUT
    ).content == bytes("SHUTDOWN", "utf-8")

    requests.put("http://localhost:5081/state", "INIT", timeout=TIMEOUT)

    assert requests.get(
        "http://localhost:5081/state", timeout=TIMEOUT
    ).content == bytes("INIT", "utf-8")
