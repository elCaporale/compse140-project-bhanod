import requests
from test_setup import *
from common import *
from httpserv import *
from io import BytesIO as IO
import os

# Skip linting tests
# pylint: skip-file

TEST_STRING = "This is an awesome test"
TEST_STRING_MULTILINE = "This is an awesome test\nwith\nmultiline\n\nyes"
TIMEOUT = 10

def test_httpserv_hostname_correct(serv_httpserv):
    assert HOSTNAME == "0.0.0.0"


def test_httpserv_correct_port(serv_httpserv):
    assert SERVERPORT_HTTPSERV == 5082


def test_httpserv_file_read_correctly(serv_httpserv):
    if not os.path.isdir("obse"):
        os.mkdir("obse")
    file = open("obse/obse.log", "w+", encoding="utf-8")
    file.write(TEST_STRING)
    file.close()

    assert requests.get("http://localhost:5082/messages", timeout=TIMEOUT).content == bytes(
        TEST_STRING, "utf-8"
    )


def test_httpserv_file_read_correctly_multiline(serv_httpserv):
    if not os.path.isdir("obse"):
        os.mkdir("obse")
    file = open("obse/obse.log", "w+", encoding="utf-8")
    file.write(TEST_STRING_MULTILINE)
    file.close()

    assert requests.get("http://localhost:5082/messages", timeout=TIMEOUT).content == bytes(
        TEST_STRING_MULTILINE, "utf-8"
    )
