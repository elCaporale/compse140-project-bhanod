from test_setup import *
from common import *

# Skip linting tests
# pylint: skip-file


def test_rabbitmq_host_correct():
    assert RABBITMQ_HOST == "rabbitmq"


def test_rabbitmq_correct_port():
    assert RABBITMQ_PORT == 5672


def test_rabbitmq_correct_creds():
    assert RABBITMQ_CREDS == pika.PlainCredentials("guest", "guest")


def test_rabbitmq_correct_routing_keys():
    assert RABBITMQ_ROUTING_KEY_O == "compse140.o"
    assert RABBITMQ_ROUTING_KEY_I == "compse140.i"
