import multiprocessing
import time
import httpserv
import orig
import pytest
import os

# Skip linting tests
# pylint: skip-file


def start_test_server_httpserv():
    process = multiprocessing.Process(target=httpserv.create_and_run_server)
    process.start()
    return process


def start_test_server_orig():
    process = multiprocessing.Process(target=orig.create_and_run_server)
    process.start()
    return process


def stop_test_server(process):
    process.kill()


@pytest.fixture
def serv_httpserv(request):
    process = start_test_server_httpserv()
    time.sleep(5)

    def teardown():
        stop_test_server(process)
        time.sleep(5)

    request.addfinalizer(teardown)

    return process


@pytest.fixture
def serv_orig(request):
    # Erase whatever state there is to reset it
    if os.path.exists("orig_state.log"):
        os.remove("orig_state.log")
    # Erase log
    if os.path.exists("orig_state_runlog.log"):
        os.remove("orig_state_runlog.log")
    process = start_test_server_orig()
    time.sleep(5)

    def teardown():
        stop_test_server(process)
        time.sleep(5)

    request.addfinalizer(teardown)

    return process
