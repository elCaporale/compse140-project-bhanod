"""Orig module."""
import threading
import time
from http.server import HTTPServer, SimpleHTTPRequestHandler
import urllib.parse
from common import compse140_connect, RABBITMQ_ROUTING_KEY_O
from common import HOSTNAME, SERVERPORT_ORIG
from orig_state import OrigState


class OrigServer(SimpleHTTPRequestHandler):
    """ORIG HTTP server class"""

    # pylint: disable=invalid-name, consider-using-with
    orig_state = None

    def __init__(self, *args, **kwargs) -> None:
        self.orig_state = OrigState()
        super().__init__(*args, **kwargs)

    def do_PUT(self):
        """PUT request handling"""
        path = urllib.parse.urlparse(self.path)[2]
        if path == "/state":
            length = int(self.headers["Content-Length"])
            new_state = self.rfile.read(length).decode("UTF-8")
            result = self.orig_state.set(new_state)

            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(
                bytes("New state set: " + str(result) + "\n", encoding="utf-8")
            )

    def do_GET(self):
        """GET request handling"""
        path = urllib.parse.urlparse(self.path)[2]
        if path == "/state":
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(bytes(self.orig_state.get(), encoding="utf-8"))
        elif path == "/run-log":
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(bytes(self.orig_state.get_run_log(), encoding="utf-8"))


def create_and_run_server():
    """Create and run HTTP server instance"""
    server_orig = HTTPServer((HOSTNAME, SERVERPORT_ORIG), OrigServer)
    threading.Thread(target=server_orig.serve_forever).start()


def serve_topic():
    """Serve RabbitMQ topic"""
    i = 0
    while True:
        time.sleep(3)
        # Read the state
        state = OrigState().get()

        # Handle state
        if state == "INIT":
            # Reset and start
            i = 0
            OrigState().set("RUNNING")
        elif state == "RUNNING":
            channel.basic_publish(
                exchange="topic_logs",
                routing_key=RABBITMQ_ROUTING_KEY_O,
                body="MSG_" + str(i),
            )
            i = i + 1


if __name__ == "__main__":
    connection = compse140_connect()
    channel = connection.channel()
    channel.exchange_declare(exchange="topic_logs", exchange_type="topic")
    create_and_run_server()
    print("Running " + HOSTNAME + ":" + str(SERVERPORT_ORIG))

    threading.Thread(target=serve_topic).start()
