"""Common module."""
import time
import pika

RABBITMQ_HOST = 'rabbitmq'
RABBITMQ_PORT = 5672
RABBITMQ_CREDS = pika.PlainCredentials("guest", "guest")
RABBITMQ_ROUTING_KEY_O = 'compse140.o'
RABBITMQ_ROUTING_KEY_I = 'compse140.i'

HOSTNAME = "0.0.0.0"
# Port for external requests
SERVERPORT = 5080
# Port for requests to ORIG
SERVERPORT_ORIG = 5081
# Port for requests to HTTPSERV
SERVERPORT_HTTPSERV = 5082


def compse140_connect():
    """Do the connection to rabbitMQ"""
    # pylint: disable= broad-except
    # Try connecting until successful
    while True:
        try:
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(
                    host=RABBITMQ_HOST,
                    port=RABBITMQ_PORT,
                    credentials=RABBITMQ_CREDS))
            if connection.is_open:
                break
        except Exception:
            time.sleep(1)
    return connection
