"""HTTPSERV module."""
from http.server import BaseHTTPRequestHandler, HTTPServer
import urllib.parse
import threading
from common import HOSTNAME, SERVERPORT_HTTPSERV


class HttpServServer(BaseHTTPRequestHandler):
    """HTTP server class"""

    # pylint: disable=invalid-name, consider-using-with
    def do_GET(self):
        """GET request handling"""
        path = urllib.parse.urlparse(self.path)[2]
        if path == "/messages":
            file = open("obse/obse.log", "r+", encoding="utf-8")
            result = file.read()
            file.close()
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(bytes(result, "utf-8"))


def create_and_run_server():
    """Create and run HTTP server instance"""
    server = HTTPServer((HOSTNAME, SERVERPORT_HTTPSERV), HttpServServer)
    threading.Thread(target=server.serve_forever).start()


if __name__ == "__main__":
    create_and_run_server()
