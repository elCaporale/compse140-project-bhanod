"""Orig state module."""
import datetime
import os

# pylint: disable=consider-using-f-string, consider-using-with


class OrigState:
    """Orig state class"""

    state = "INIT"

    def __init__(self) -> None:
        if os.path.exists("orig_state.log"):
            file = open("orig_state.log", "r+", encoding="utf-8")
            self.state = file.read()
            file.close()
        else:
            # Set state
            file = open("orig_state.log", "a", encoding="utf-8")
            file.write("%s" % (self.state))
            file.close()
            # Log initial state
            timestamp = datetime.datetime.utcnow().isoformat()[:-3] + "Z"
            file = open("orig_state_runlog.log", "a", encoding="utf-8")
            file.write("%s: %s\n" % (timestamp, self.state))
            file.close()

    def validate_state(self, new_state):
        """Validate state before transition"""
        print(new_state, self.state)
        if self.state == new_state:
            return False

        return bool(new_state in ("INIT", "PAUSED", "RUNNING", "SHUTDOWN"))

    def log_transition(self, new_state):
        """Log transition to file"""
        timestamp = datetime.datetime.utcnow().isoformat()[:-3] + "Z"
        file = open("orig_state_runlog.log", "a", encoding="utf-8")
        file.write("%s: %s\n" % (timestamp, new_state))
        file.close()
        print("transition successful")

    def set(self, new_state):
        """Set new state"""
        if self.validate_state(new_state):
            self.log_transition(new_state)
            file = open("orig_state.log", "w+", encoding="utf-8")
            file.write("%s" % (new_state))
            file.close()
            self.state = new_state
            return True

        print("transition failed")
        return False

    def get(self):
        """Get the state"""
        return self.state

    def get_run_log(self):
        """Get run log"""
        file = open("orig_state_runlog.log", "r+", encoding="utf-8")
        result = file.read()
        file.close()
        return result
