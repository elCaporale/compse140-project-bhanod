"""GATEWAY module."""
import http
from http.server import BaseHTTPRequestHandler, HTTPServer
import urllib.parse
from common import HOSTNAME, SERVERPORT, SERVERPORT_ORIG, SERVERPORT_HTTPSERV

class GatewayServer(BaseHTTPRequestHandler):
    """HTTP server class"""
    # pylint: disable=invalid-name, consider-using-with
    def do_GET(self):
        """GET request handling"""
        path  = urllib.parse.urlparse(self.path)[2]
        if path == "/state":
            connection = http.client.HTTPConnection("orig:" + str(SERVERPORT_ORIG))
            connection.request("GET", path)
            result = connection.getresponse().read()

            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(result)
        elif path == "/messages":
            connection = http.client.HTTPConnection("httpserv:" + str(SERVERPORT_HTTPSERV))
            connection.request("GET", path)
            result = connection.getresponse().read()

            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(result)
        elif path == "/run-log":
            connection = http.client.HTTPConnection("orig:" + str(SERVERPORT_ORIG))
            connection.request("GET", path)
            result = connection.getresponse().read()

            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(result)

    def do_PUT(self):
        """PUT request handling"""
        path  = urllib.parse.urlparse(self.path)[2]
        if path == "/state":
            connection = http.client.HTTPConnection("orig:" + str(SERVERPORT_ORIG))
            length = int(self.headers["Content-Length"])
            data = self.rfile.read(length)
            connection.request("PUT", path, data)
            result = connection.getresponse().read()

            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(result)


def create_and_run_server():
    """Create and run HTTP server instance"""
    server = HTTPServer((HOSTNAME, SERVERPORT), GatewayServer)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass

    server.server_close()


if __name__ == "__main__":
    create_and_run_server()
