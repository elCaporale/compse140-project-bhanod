"""Imed module."""
import time
from common import compse140_connect, RABBITMQ_ROUTING_KEY_I, RABBITMQ_ROUTING_KEY_O

def callback(channel, method, properties, body):
    """Connection channel feedback"""
    # pylint: disable=unused-argument
    time.sleep(1)
    channel.basic_publish(
        exchange="topic_logs",
        routing_key=RABBITMQ_ROUTING_KEY_I,
        body="Got " + body.decode(),
    )


if __name__ == "__main__":
    connection = compse140_connect()
    channel_o = connection.channel()
    channel_o.exchange_declare(exchange="topic_logs", exchange_type="topic")
    channel_i = connection.channel()
    channel_i.exchange_declare(exchange="topic_logs", exchange_type="topic")

    result = channel_o.queue_declare("", exclusive=False)
    queue_name = result.method.queue

    channel_o.queue_bind(
        exchange="topic_logs", queue=queue_name, routing_key=RABBITMQ_ROUTING_KEY_O
    )
    channel_o.basic_consume(
        queue=queue_name, on_message_callback=callback, auto_ack=True
    )

    channel_o.start_consuming()
