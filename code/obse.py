"""Obse module."""
import datetime
import os
from common import compse140_connect

def callback(chan, method, properties, body):
    """Connection channel feedback"""
    # pylint: disable=unused-argument, consider-using-f-string, consider-using-with, global-statement, used-before-assignment
    global OBSERVED_MSG
    timestamp = datetime.datetime.utcnow().isoformat()[:-3] + "Z"
    OBSERVED_MSG = OBSERVED_MSG + 1
    file = open("obse/obse.log", "a", encoding="utf-8")
    file.write(
        "%s %d %s to %s\n" % (timestamp, OBSERVED_MSG, body.decode(), str(method.routing_key))
    )
    file.close()


if __name__ == "__main__":
    if os.path.exists("obse/obse.log"):
        os.remove("obse/obse.log")
    OBSERVED_MSG = 0

    connection = compse140_connect()
    channel = connection.channel()
    channel.exchange_declare(exchange="topic_logs", exchange_type="topic")

    result = channel.queue_declare("", exclusive=False)
    queue = result.method.queue

    channel.queue_bind(exchange="topic_logs", queue=queue, routing_key="#")
    channel.basic_consume(queue=queue, on_message_callback=callback, auto_ack=True)

    channel.start_consuming()
